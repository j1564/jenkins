const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/', (request, response) => {
	const query = ` select * from employee `

	db.execute(query, (error, result) => {
		response.send(utils.createResult(error, result))
	})
})

router.delete('/:id', (request, response) => {
	const { id } = request.params

	const query = ` delete from employee where empid = ${id} `

	db.execute(query, (error, result) => {
		response.send(utils.createResult(error, result))
	})
})

router.post('/', (request, response) => {
	const { name, salary, age } = request.body

	const query = ` insert into employee values (default, '${name}', ${salary}, ${age}) `

	db.execute(query, (error, result) => {
		response.send(utils.createResult(error, result))
	})
})

router.put('/:id', (request, response) => {
	const { id } = request.params
	const { name, salary, age } = request.body

	const query = ` update employee set name = '${name}', salary = ${salary}, age = ${age} where empid = ${id} `

	db.execute(query, (error, result) => {
		response.send(utils.createResult(error, result))
	})
})

module.exports = router