create table employee (
    empid integer primary key auto_increment,
    name VARCHAR(40),
    salary float,
    age integer
);

insert into employee values (default, "abc", 5000, 15);
insert into employee values (default, "def", 7000, 19);
insert into employee values (default, "ghi", 2000, 14);